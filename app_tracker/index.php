<?php

if ( !isset( $_GET[ 'partner_id' ] ) ) exit;

require 'library/redis/autoload.php';

Predis\Autoloader::register();

$oRedisClient = new Predis\Client();

$aProcessData = $_SERVER;

$aProcessData[ 'partner_id' ] = (int)$_GET[ 'partner_id' ];

$oRedisClient->rpush( 'st', [ json_encode( $aProcessData ) ] );