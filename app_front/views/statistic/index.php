<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StatisticSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Статистика';
?>
<div class="statistic-index">

    <h1><?= Html::encode( $this->title ) ?></h1>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget( [
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'p_id',
                'header' => 'Партнер ID',
                'value'      => function ( $data ) {
                    return $data['p_id'];
                }
            ],
            [
                'header' => 'Дата',
                'filter'=>\yii\jui\DatePicker::widget(
                    [
                        'dateFormat' => 'dd.MM.yyyy',
                        'model' => $searchModel,
                        'attribute' => 'date',
                        'options' => [
                            'class' => 'form-control',
                        ],
                    ]),
                'value'      => function ( $data ) {
                    return date( 'd.m.Y', $data['date'] );
                }
            ],
            [
                'header' => 'Время',
                'filter'=>false,
                'value'      => function ( $data ) {
                    return $data['h'];
                }
            ],
            [
                'header' => 'Визиты',
                'filter'=>false,
                'value'      => function ( $data ) {
                    return $data['visits'];
                }
            ],
            [
                'header' => 'Уникальные визиты',
                'filter'=>false,
                'value'      => function ( $data ) {
                    return $data['unique_visits'];
                }
            ],
        ],
    ] ); ?>
</div>