<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\StatisticSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="statistic-search">

    <?php $form = ActiveForm::begin( [
        'action' => [ 'index' ],
        'method' => 'get',
    ] ); ?>

    <?= $form->field( $model, 'p_id' ) ?>

    <?= $form->field( $model, 'operator' )->dropDownList( \app\models\Statistic::getMapOperators(), [ 'prompt' => 'Выберите оператора' ] ); ?>
    <?= $form->field( $model, 'country' )->dropDownList( \app\models\Statistic::getMapCountries(), [ 'prompt' => 'Выберите страну' ] ); ?>
    <div class="form-group">
    <?= Html::activeLabel( $model, 'date' ) ?>
    <?=
    \yii\jui\DatePicker::widget(
        [
            'dateFormat' => 'dd.MM.yyyy',
            'model'      => $model,
            'attribute'  => 'date',
            'options'    => [
                'id'=>'date_2',
                'class' => 'form-control',
            ],
        ] ); ?>
    </div>
    <div class="form-group">
        <?= Html::submitButton( 'Поиск', [ 'class' => 'btn btn-primary' ] ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
