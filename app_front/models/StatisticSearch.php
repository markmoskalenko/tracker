<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\components\sphinx\Query;

/**
 * StatisticSearch represents the model behind the search form about `app\models\Statistic`.
 */
class StatisticSearch extends Statistic {

    protected static $_cacheDependency;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [ [ 'id', 'h', 'p_id' ], 'integer' ],
            [ [ 'ip', 'u_a', 'country', 'operator', 'date' ], 'safe' ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Условие сброса кеша
     *
     * @return \yii\caching\DbDependency
     */
    public static function getCacheDependency() {
        if ( static::$_cacheDependency === null ) {
            static::$_cacheDependency = new \yii\caching\TagDependency( [ 'tags' => [ 'tag_statistic' ] ] );
        }

        return static::$_cacheDependency;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search( $params ) {

        $dataProvider = new ActiveDataProvider( [
            'query' => $this->getQuery( $params ),
        ] );

        return $dataProvider;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuery( $params ) {

        $query = new Query();


        $query->select( [ '*', 'COUNT(DISTINCT ip) as unique_visits', 'COUNT(*) as visits' ] );

        $query->from( [ 'tbl_statistic' ] );

        $query->addGroupBy(['p_id', 'date']);

        $this->load( $params );

        if ( !$this->validate() ) {
            return $query;
        }

        $query->andFilterWhere( [
            'p_id'     => $this->p_id,
            'date'     => $this->date ? strtotime( $this->date ) : null,
            'operator' => $this->operator,
            'country'  => $this->country
        ] );

//        $query->match(['operator'=>[$this->operator]]);

        return $query;
    }
}