<?php

namespace app\models;

use app\commands\ProcessingStatisticController;
use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "tbl_statistic".
 *
 * @property string $id
 * @property string $ip
 * @property string $u_a
 * @property integer $p_id
 * @property string $country
 * @property string $operator
 */
class Statistic extends \yii\db\ActiveRecord {

    public $visits;
    public $unique_visits;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'tbl_statistic';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [ [ 'ip', 'date', 'h', 'u_a', 'p_id', 'country', 'operator' ], 'required' ],
            [ [ 'date', 'h', 'p_id' ], 'integer' ],
            [ [ 'ip' ], 'string', 'max' => 50 ],
            [ [ 'u_a' ], 'string', 'max' => 255 ],
            [ [ 'country', 'operator' ], 'string', 'max' => 10 ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id'            => 'ID',
            'p_id'          => 'Партнер ID',
            'ip'            => 'Ip',
            'u_a'           => 'Юзер Агент',
            'country'       => 'Страна',
            'operator'      => 'Оператор',
            'date'          => 'Дата',
            'h'             => 'Время',
            'visits'        => 'Визиты',
            'unique_visits' => 'Уникальные',
        ];
    }

    public static function getMapOperators(){
        $aOperators = Json::decode(ProcessingStatisticController::$sOperators);
        $aOperatorsMap = [];
        foreach ( $aOperators as $aItem ) {
            $aOperatorsMap[$aItem['name']]=$aItem['name'];
        }
        return $aOperatorsMap;
    }

    public static function getMapCountries(){
        $aOperators = Json::decode(ProcessingStatisticController::$sOperators);
        $aOperatorsMap = [];
        foreach ( $aOperators as $aItem ) {
            $aOperatorsMap[$aItem['country_code']]=$aItem['country_code'];
        }
        return $aOperatorsMap;
    }
}