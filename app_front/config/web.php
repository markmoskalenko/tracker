<?php

$params = require( __DIR__ . '/params.php' );

$config = [
    'id'           => 'basic',
    'basePath'     => dirname( __DIR__ ),
    'defaultRoute' => 'statistic/index',
    'language'     => 'ru',
    'components'   => [
        'sphinx' => [
            'class' => 'yii\sphinx\Connection',
            'dsn' => 'mysql:host=127.0.0.1;port=9306;',
            'username' => 'admin_mark',
            'password' => 'Funnyday5',
            'tablePrefix'=>'tbl_',
        ],
        'redis'        => [
            'class'    => 'yii\redis\Connection',
            'hostname' => 'localhost',
            'port'     => 6379,
            'database' => 0,
        ],
        'request'      => [
            'cookieValidationKey' => 'W0qaFttwlTwmidCAlFgimZx1FBfoxGjO',
        ],
        'cache'        => [
            'class' => 'yii\redis\Cache',
        ],
        'urlManager'   => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'rules'           => [
                '<controller:\w+>/<id:\d+>'              => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>'          => '<controller>/<action>',
                'debug/<controller>/<action>'            => 'debug/<controller>/<action>',
            ]
        ],
        'session'      => [
            'class' => 'yii\redis\Session',
        ],
        'errorHandler' => [
            'errorAction' => 'statistic/error',
        ],
        'db'           => require( __DIR__ . '/db.php' ),
    ],
    'params'       => $params,
];

return $config;