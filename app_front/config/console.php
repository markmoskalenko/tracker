<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

return [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'app\commands',
    'components' => [
        'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => 'localhost',
            'port' => 6379,
            'database' => 0,
        ],

        'cache' => [
            'class' => 'yii\redis\Cache',
        ],
        'session'      => [
            'class' => 'yii\redis\Session',
        ],
        'db' => $db,
    ],
    'params' => $params,
];
