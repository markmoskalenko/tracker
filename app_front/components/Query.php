<?php

namespace app\components;

use Yii;

class Query extends \yii\db\Query
{
    // /**
    //  * Queries a scalar value by setting [[select]] first.
    //  * Restores the value of select to make this query reusable.
    //  * @param string|Expression $selectExpression
    //  * @param Connection|null $db
    //  * @return bool|string
    //  */
    // protected function queryScalar($selectExpression, $db)
    // {
    //     $select = $this->select;
    //     $limit = $this->limit;
    //     $offset = $this->offset;

    //     $this->select = [$selectExpression];
    //     $this->limit = null;
    //     $this->offset = null;
    //     $command = $this->createCommand($db);

    //     $this->select = $select;
    //     $this->limit = $limit;
    //     $this->offset = $offset;

    //     if (empty($this->groupBy) && empty($this->union) && !$this->distinct) {
    //         return $command->queryScalar();
    //     } else {
    //         return (new static)->select([$selectExpression])
    //             ->from(['c' => $this])
    //             ->createCommand($command->db)
    //             ->queryScalar();
    //     }
    // }
}
