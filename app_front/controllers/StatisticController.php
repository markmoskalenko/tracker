<?php

namespace app\controllers;

use Yii;
use app\models\Statistic;
use app\models\StatisticSearch;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
        use yii\sphinx\Query;

/**
 * StatisticController implements the CRUD actions for Statistic model.
 */
class StatisticController extends Controller
{
    public function actions() {
        return [
            'error'   => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }


    /**
     * Lists all Statistic models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new StatisticSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionViewRedis()
    {
        $aProcessData = \Yii::$app->redis->executeCommand( 'LRANGE', [ 'st', 0, 10000 ] );
        echo '<pre>';
        print_r($aProcessData);
        echo '</pre>';
        Yii::$app->end();
    }


    /**
     * Finds the Statistic model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Statistic the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Statistic::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
