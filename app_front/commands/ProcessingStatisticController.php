<?php

namespace app\commands;

use yii\console\Controller;
use yii\helpers\Json;

class ProcessingStatisticController extends Controller {

    /**
     * Пулл операторов
     * @var string
     */
    public static $sOperators = '
    [
     {
        "name": "MTS",
        "country_code": "RUS",
        "subnets": [
            "88.198.247.4/16",
            "78.47.58.4/16",
            "136.243.248.190/16",
            "148.251.188.252/16"
        ]
    },
    {
        "name": "BLN",
        "country_code": "USA",
        "subnets": [
            "136.243.248.180/16",
            "144.76.17.26/16",
            "144.76.17.29/16"
        ]
    }
    ]';

    /**
     * Обработчик входных данных
     */
    public function actionIndex() {

        ini_set( 'memory_limit', '-1' );
        ini_set( 'max_execution_time', '-1' );
        date_default_timezone_set( 'Europe/Minsk' );

        $aProcessData = \Yii::$app->redis->executeCommand( 'LRANGE', [ 'st', 0, 100000 ] );

        if ( !count( $aProcessData ) ) return;

        \Yii::$app->redis->executeCommand( 'LTRIM', [ 'st', ( count( $aProcessData ) ), -1 ] );

        $aInsertCommand = [ ];

        foreach ( $aProcessData as $sItemData ) {

            $aItemData = Json::decode( $sItemData, true );

            $sIp = $this->getIp( $aItemData );

            $iDate = mktime(0,0,0,date('m'), date('d'), date('Y'));

            $iHour = date('G');

            $sUserAgent = $this->getUserAgent( $aItemData );

            $aOperatorAndCountry = $this->getOperatorAndCountry( $sIp );

            $sOperator = $aOperatorAndCountry[ 'operator' ];

            $sCountry = $aOperatorAndCountry[ 'country_code' ];

            $iPartnerId = $this->getPartnerId( $aItemData );

            $aInsertCommand[ ] = "('{$sIp}', '{$sUserAgent}', '{$iPartnerId}', '{$sCountry}', '{$sOperator}', '{$iDate}', '{$iHour}')";
        }

        $sSql = "INSERT INTO `tbl_statistic` (`ip`, `u_a`, `p_id`, `country`, `operator`, `date`, `h`) VALUES " . implode( ',', $aInsertCommand );

         \Yii::$app->db->createCommand( $sSql )->query();
    }

    public function actionTest() {

        ini_set( 'memory_limit', '-1' );
        ini_set( 'max_execution_time', '-1' );
        date_default_timezone_set( 'Europe/Minsk' );

        $aProcessData = \Yii::$app->redis->executeCommand( 'LRANGE', [ 'st', 0, 100000 ] );

        if ( !count( $aProcessData ) ) return;

//        \Yii::$app->redis->executeCommand( 'LTRIM', [ 'st', ( count( $aProcessData ) ), -1 ] );

        $aInsertCommand = [ ];

        foreach ( $aProcessData as $sItemData ) {

            $aItemData = Json::decode( $sItemData, true );

            $sIp = $this->getIp( $aItemData );

            $iDate = mktime(0,0,0,rand(1,12), rand(1,29), rand(2014,2016));

            $iHour = rand(0,23);//date('G');

            $sUserAgent = $this->getUserAgent( $aItemData );

            $aOperatorAndCountry = $this->getOperatorAndCountry( $sIp );

            $sOperator = $aOperatorAndCountry[ 'operator' ];

            $sCountry = $aOperatorAndCountry[ 'country_code' ];

            $iPartnerId = rand(1,100);//$this->getPartnerId( $aItemData );

            $aInsertCommand[ ] = "('{$sIp}', '{$sUserAgent}', '{$iPartnerId}', '{$sCountry}', '{$sOperator}', '{$iDate}', '{$iHour}')";
        }

        $sSql = "INSERT INTO `tbl_statistic` (`ip`, `u_a`, `p_id`, `country`, `operator`, `date`, `h`) VALUES " . implode( ',', $aInsertCommand );

        \Yii::$app->db->createCommand( $sSql )->query();
        \Yii::$app->db->createCommand( $sSql )->query();
        \Yii::$app->db->createCommand( $sSql )->query();
        \Yii::$app->db->createCommand( $sSql )->query();
        \Yii::$app->db->createCommand( $sSql )->query();
        \Yii::$app->db->createCommand( $sSql )->query();
        \Yii::$app->db->createCommand( $sSql )->query();
        \Yii::$app->db->createCommand( $sSql )->query();
        \Yii::$app->db->createCommand( $sSql )->query();
        \Yii::$app->db->createCommand( $sSql )->query();
        \Yii::$app->db->createCommand( $sSql )->query();
        \Yii::$app->db->createCommand( $sSql )->query();
        \Yii::$app->db->createCommand( $sSql )->query();
        \Yii::$app->db->createCommand( $sSql )->query();
        \Yii::$app->db->createCommand( $sSql )->query();
        \Yii::$app->db->createCommand( $sSql )->query();
        \Yii::$app->db->createCommand( $sSql )->query();
        \Yii::$app->db->createCommand( $sSql )->query();
        \Yii::$app->db->createCommand( $sSql )->query();
        \Yii::$app->db->createCommand( $sSql )->query();

        $this->actionTest();
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    public function getIp( array $data ) {
        if ( !empty( $data[ 'HTTP_CLIENT_IP' ] ) ) {
            return $data[ 'HTTP_CLIENT_IP' ];
        } elseif ( !empty( $data[ 'HTTP_X_FORWARDED_FOR' ] ) ) {
            return $data[ 'HTTP_X_FORWARDED_FOR' ];
        } else {
            return $data[ 'REMOTE_ADDR' ];
        }
    }

    /**
     * Получаем ID партнера из массива $_SERVER
     *
     * @param array $data
     *
     * @return int
     */
    public function getPartnerId( array $data ) {
        return (int)$data[ 'partner_id' ];
    }

    /**
     * Получаем UA из массива $_SERVER
     *
     * @param array $data
     *
     * @return mixed
     */
    public function getUserAgent( array $data ) {
        return $data[ 'HTTP_USER_AGENT' ];
    }

    /**
     *
     * Получаем оператора по маске и страну из массива $_SERVER
     *
     * @param $sIp
     *
     * @return array
     */
    public function getOperatorAndCountry( $sIp ) {

//        if ( $r = \Yii::$app->cache->get( $sIp ) ) {
//
//            return Json::decode( $r, true );
//
//        } else {
            $aOperators = Json::decode( static::$sOperators );

            foreach ( $aOperators as $aOperator ) {

                foreach ( $aOperator[ 'subnets' ] as $sNetwork ) {

                    if ( $this->netMatch( $sNetwork, $sIp ) ) {

                        $aResult = [
                            'country_code' => $aOperator[ 'country_code' ],
                            'operator'     => $aOperator[ 'name' ]
                        ];

                        \Yii::$app->cache->set( $sIp, Json::encode( $aResult ) );

                        return $aResult;
                    }
                }
//            }

            return [
                'country_code' => '',
                'operator'     => '',
            ];
        }
    }

    /**
     * Проверяем отношение IP к маске подсети
     *
     * @param $network
     * @param $ip
     *
     * @return bool
     */
    private function netMatch( $network, $ip ) {
        $network = trim( $network );
        $orig_network = $network;
        $ip = trim( $ip );
        if ( $ip == $network ) {
            return true;
        }
        $network = str_replace( ' ', '', $network );
        if ( strpos( $network, '*' ) !== false ) {
            if ( strpos( $network, '/' ) !== false ) {
                $asParts = explode( '/', $network );
                $network = @ $asParts[ 0 ];
            }
            $nCount = substr_count( $network, '*' );
            $network = str_replace( '*', '0', $network );
            if ( $nCount == 1 ) {
                $network .= '/24';
            } else if ( $nCount == 2 ) {
                $network .= '/16';
            } else if ( $nCount == 3 ) {
                $network .= '/8';
            } else if ( $nCount > 3 ) {
                return true; // if *.*.*.*, then all, so matched
            }
        }

        $d = strpos( $network, '-' );
        if ( $d === false ) {
            $ip_arr = explode( '/', $network );
            if ( !preg_match( "@\d*\.\d*\.\d*\.\d*@", $ip_arr[ 0 ], $matches ) ) {
                $ip_arr[ 0 ] .= ".0";    // Alternate form 194.1.4/24
            }
            $network_long = ip2long( $ip_arr[ 0 ] );
            $x = ip2long( $ip_arr[ 1 ] );
            $mask = long2ip( $x ) == $ip_arr[ 1 ] ? $x : ( 0xffffffff << ( 32 - $ip_arr[ 1 ] ) );
            $ip_long = ip2long( $ip );

            return ( $ip_long & $mask ) == ( $network_long & $mask );
        } else {
            $from = trim( ip2long( substr( $network, 0, $d ) ) );
            $to = trim( ip2long( substr( $network, $d + 1 ) ) );
            $ip = ip2long( $ip );

            return ( $ip >= $from and $ip <= $to );
        }
    }
}